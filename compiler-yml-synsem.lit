@title Assembling the synsem compiler

@s Introduction
This documentation explains how to assemble a XMG-2 compiler with a unique description file. This file, called `compiler.yml` specifies the set of bricks used to build the compiler and indicates how these bricks should be connected. Writing such a description might seem complicated, because one needs to be aware of what each XMG-2 brick contains. However, from one compiler to the other, the `compiler.yml` remains quite similar: the most critical point is the choice of the dimensions. To get advices to create easily your own compilers from an existing one, see the conclusion.

@s Content of the file

Let us start with the most standard brick, called `mg`, which contains the XMG fundamental language: a set of declarations (which can be of different types), followed by a set of classes.
--- mg_rules
mg: 
  _Stmt: control
  _EDecls: decls
---
We create an instance of the brick `mg`. This brick has 2 external non-terminals (see the `lang.def` file of the brick `mg`). The first one of these external non-terminals should give the shape of the statements in the classes (`_Stmt`), the second one should give the type of declarations (as types for example) coming at the beginning of the metagrammar. We choose here the standard bricks for these needs: `control` is the control language of XMG (conjunctions, disjunctions, class calls), and `decls` contains the support for basic type definitions and principles. Now we need to connect the external non-terminals of these two bricks.
--- decls_rules
decls:
  _ODecl: feats sem.Macro
---
The external non-terminal `_ODecl` allows to add other declarations to the standard type and principle ones. The brick `feats` gives support for the typing of features (as in XMG-1), and `sem` is the brick for predicate semantics. This brick contains the non-terminal `Macro`, which can parse `extern` and `semantics` statements.
--- control_rules
control:
  _Stmt: dim_sem dim_syn dim_iface stardim
  _Expr: value
---
The `control` brick also has two external non terminals. `_Stmt` is the language of the statement combined with the logic language and `_Expr` is the language of the expressions used in equality statements. For the second one, we use the brick `value` which allows to parse expressions such as identifiers, integers or booleans. The `_Stmt` brick needs to be connected to all the possible description languages offered by the compiler, so this is the essential part of this `compiler.yml` file. We connect the `_Stmt` non-terminal to four bricks: the first three bricks are different instances of the parametrized brick `dim`, allowing to create languages for dimensions. The fourth brick, `stardim`, adds to the language the `*=[...]` syntax for interfaces (as used in XMG-1). 
--- stardim_rules
stardim:
  _Stmt: control
  proxy: dim_iface
---
The brick `stardim` is parametrized. It means that here, the task is not only to connect external non-terminals, but also to give values to parameters. `_Stmt` gives the grammar of anything that comes on the left of the `*=[...]` operator. We use for this `control`, because we want to be able to attach interfaces to any statement defined previously. The proxy parameter indicates which dimension is associated to the syntactic sugar `*=[...]`, therefore we give as value to this parameter our dimension `dim_iface`.

Now, we give the connections for the `avm` brick, needed to describe feature structures (in the interface and the syn dimension for instance).
--- avm_rules
avm: 
  _Value: value
  _Expr: value_expr
---
The external non-terminal `_Value` corresponds to the values associated to the attributes, we use the same instance of the `value` brick as previously. The external non-terminal `_Expr` is the shape of the expressions when using the "dot" notation to access values of specific attributes in an AVM. For this, we use a fresh instance of the `value` brick (our first instance will be extended soon and we need only a basic value language here).
--- value_rules
value: 
  _Else: avm adisj avm.Dot control.Call
---
Our first `value` brick, which we used for values in feature structures and expressions in the `control` brick, has to be extended. We connect the external non-terminal `_Else` to all additional types of values we need: `avm` to allow feature structures of arbitrary depth, `adisj` for the atomic disjunctions (`\@{np,cl}` for example), the non-terminal `Dot` from the brick `avm` to add the "dot" access to attributes' values, and the non-terminal `Call` from `control` to allow for classes instances.

The next bricks to connect are the description languages.
--- syn_rules
syn:
  _AVM: avm
---
In the `syn` brick, the only external non-terminal is `_AVM`, for the language of features structures associated to nodes. We use our previously defined `avm` brick.
--- iface_rules
iface:
  _AVM: avm
---
The same is done for the `iface` description language.

Finally, we need to plug and parametrize the dimension bricks. We need to give four pieces of information:
* `tag` is the identifier which will be used in the metagrammar to refer to the dimension (as `syn` in `<syn>{ ... }`)
* `solver` is the name of the solver used to extract models of the descriptions in the dimension
* `Stmt` is the brick containing the description language of the dimension (basically the `_Stmt` of the `control` brick used in this dimension)
* `Expr` is the brick containing the language for expressions in the dimensions (the `_Expr` of the `control` brick in this dimension)
--- dim_syn_rules
dim_syn:
  tag: "syn"
  solver: "tree"
  Stmt: syn
  Expr: value
---
For the `<syn>` dimension, we use obviously the tag `syn` and the brick `syn`. The language of expressions is defined in our brick `value`. The solver used is `tree`, which, as its name indicates, extract all tree models of the descriptions.
--- dim_sem_rules
dim_sem:
  tag: "sem"
  Stmt: sem
  Expr: value
---
The same is done for the `sem` dimension, except that in this case, we do not specify a solver (meaning that the solver is equivalent to the identity relation).
--- dim_iface_rules
dim_iface:
  tag: "iface"
  Stmt: iface
  Expr: value
---
Finally, the `iface` dimension is built on the same model (the solver is the identity relation again).

@s Conclusion
This documentation describes the creation of the `synsem` compiler. To create a new compiler, the structure of the `compiler.yml` file will be very similar. In the standard case, the dimensions and their description languages are the only parts which need to be modified. For example, the compilers `synframe` and `syn2frame` might be interesting to look at: in the first one, only the language for semantics change, and in the second one, a slighlty different tree description language is used. 

It is also useful to note that no principle appears in this compiler description: principles are installed with the solvers and are always useable when the solver is associated to a dimension. This means that in the `synsem` compiler, all principles compatible with the `tree` solver will be available for the `<syn>` dimension.


--- compiler-synsem.yml
@{mg_rules}
@{decls_rules}
@{control_rules}
@{stardim_rules}
@{avm_rules}
@{value_rules}
@{syn_rules}
@{iface_rules}
@{dim_syn_rules}
@{dim_sem_rules}
@{dim_iface_rules}
---