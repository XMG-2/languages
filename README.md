## About this repository

This gitlab project hosts documented files used to extend XMG-2.

These lang.def and compiler.yml files are documented by using the [lit](https://github.com/zyedidia/Literate) literate programming tool.

Basically, instead of maintaining two (sets of) files: one for the source code, one for the documentation, everything goes to the same place: the `.lit` file. From this file, both an HTML-based documentation and a lang.def or compiler.yml file are compiled.

## How to contribute ?

Once you cloned this repository, you can add your own metagrammar by:

1. insert it in a `.lit` file (say `my_mg.lit`) whose content follow the lit syntax as described in the lit [manual](http://literate.zbyedidia.webfactional.com/manual.php)
2. add a reference to your `.lit` file in `main.lit`
3. recompile the full documentation via `lit main.lit`
4. git add / commit / push the resulting files

## Where to read the documentation ?

Once you git pushed your modifications, you can read the documentation from [https://xmg-2.gitlab.io/languages/XMG-2%20languages_contents.html](https://xmg-2.gitlab.io/languages/XMG-2%20languages_contents.html)
